export class HttpException extends Error {
    constructor(public message: string,public status: number) {
      super(message);
    }
}

export interface responseErrorMessage{
  statusError: number;
  message: string;
  stack?: string | undefined;
}