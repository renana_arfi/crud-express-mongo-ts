import {RequestHandler } from "express";
import {uniqeID} from "../utils/string.js"

export const addIdToRequest:RequestHandler =  ( req, res, next) => {
    req.id = uniqeID();
    next();
}