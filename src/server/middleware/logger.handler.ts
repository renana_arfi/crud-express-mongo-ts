import { Request, Response, NextFunction } from "express";
import {HttpException} from "../utils/error.types.js"

import fs from "fs/promises";

//middelware - update log http-requests 
export const updateLogFile = (path: string) => (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    fs.writeFile(path, `req id - ${req.id} = ${req.method}: ${req.path} >> ${Date.now()}\n`, {
      flag: "a",
    });
    next();
  };

//middelware - update Log-errors
export const updateLogError= (path:string) => (err:HttpException,req:Request,res:Response,next:NextFunction)=>{
    fs.writeFile(path,`req id - ${req.id} = ${err.status} :: ${err.message} >> ${err.stack} \n`
    , {
        flag: "a",
      });
      next(err);
}