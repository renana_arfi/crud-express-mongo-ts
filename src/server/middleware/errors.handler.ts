import log from '@ajar/marker';
import {ErrorRequestHandler,Request,Response,NextFunction} from "express"
import {responseErrorMessage,HttpException} from "../utils/error.types.js"
const { NODE_ENV } = process.env;

//middelware  - print the Error to the console
export const printError:ErrorRequestHandler =  (err:HttpException, req, res, next) => {
    log.error(err);
    next(err)
}

//middelware - response Error to client
export const responseError:ErrorRequestHandler =  (err:HttpException, req, res, next) => {
    let errRes: responseErrorMessage;
    errRes = {statusError:err.status,message:err.message};
    if(NODE_ENV !== 'production') errRes.stack = err.stack;
    res.status(errRes.statusError).json(errRes);
}

//middleware - when url not found and we want send httpException
export const not_found =  (req:Request, res:Response,next:NextFunction) => {
    const full_url = new URL(req.url, `http://${req.headers.host}`);
    next(new HttpException(` - 404 - url ${full_url.href } was not found`,404))
}



