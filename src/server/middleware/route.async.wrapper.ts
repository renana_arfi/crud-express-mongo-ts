import {Request,Response,NextFunction,} from 'express'
import {HttpException} from "../utils/error.types.js"

export default (fn:Function) => (req:Request, res:Response, next:NextFunction) => {
      fn(req,res,next).catch((err:any)=>{
          if(err.status) next(err);
          else next(new HttpException(err.message,500))
      });
};
