// require('dotenv').config();
import express, { Express } from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { updateLogError, updateLogFile } from "./middleware/logger.handler.js";
import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.router.js";
import { addIdToRequest } from "./middleware/request.handler.js";
import {
    printError,
    responseError,
    not_found,
} from "./middleware/errors.handler.js";

const LOG_FILE_PATH = "./req.http.log";
const LOG_ERROR_PATH = "./req.error.log";
const {
    PORT = 8080,
    HOST = "localhost",
    DB_URI = "mongodb://localhost:27017/crud-demo",
} = process.env;

class api {
    constructor() {
        const app = express();
        this.genericMiddleware(app);
        this.action(app);
        this.errorHandling(app);
        //start the express api server
        this.init(app);
    }

    genericMiddleware(app: Express) {
        app.use(cors());
        app.use(morgan("dev"));
    }

    action(app: Express) {
        app.use(addIdToRequest);
        app.use(updateLogFile(LOG_FILE_PATH));
        app.use("/api/users", user_router);
    }

    errorHandling(app: Express) {
        app.use(not_found);
        app.use(printError);
        app.use(updateLogError(LOG_ERROR_PATH));
        app.use(responseError);
    }

    async init(app: Express) {
        try {
            //connect to mongo db
            await connect_db(DB_URI);
            await app.listen(Number(PORT), HOST);
            log.magenta(
                "api is live on",
                ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`
            );
        } catch (err) {
            console.log(err);
        }
    }
}

new api();
