import Joi from "joi";
import { HttpException } from "../../utils/error.types.js";
import { User } from "../../utils/user.types.js";

export const validateUser = async (value: User, type: string) => {
    try {
        let first_name = Joi.string().alphanum().min(3).max(30);
        let last_name = Joi.string().alphanum().min(3).max(30);
        let email = Joi.string().email({
            minDomainSegments: 2,
            tlds: { allow: ["com", "net"] },
        });
        let phone = Joi.string().regex(/^\d{3}-\d{3}-\d{4}$/);

        if (type === "createUser") {
            first_name = first_name.required();
            last_name = last_name.required();
            email = email.required();
            phone = phone.required();
        }

        const schema = Joi.object().keys({
            first_name,
            last_name,
            email,
            phone,
        });

        const user = await schema.validateAsync(value);
        return user;

    } catch (err:any) {
        throw new HttpException(err.details[0].message, 400);
    }
};
