import user_model from "./user.model.js";
import { User } from "../../utils/user.types.js";

export class userService {
    async readAllUsers(): Promise<User[]> {
        const users = await user_model.find().select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
        return users;
    }

    async readUser(idToRead: string): Promise<User> {
        const user = await user_model.findById(idToRead);
        return user;
    }

    async deleteUser(idToDelete: string) {
        const user = await user_model.findByIdAndRemove(idToDelete);
        return user;
    }

    async createUser(user: User) {
        const newUser = await user_model.create(user);
        return newUser;
    }

    async updateUser(idToUpdate: string, user: User) {
        const newUser = await user_model.findByIdAndUpdate(idToUpdate, user, {
            new: true,
            upsert: false,
        });
        return newUser;
    }
}
