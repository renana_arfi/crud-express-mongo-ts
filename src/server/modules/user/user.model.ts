import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : String,
    last_name   : { type:String,required:[true,'First name is required'] },
    email       : { type:String,required:true,unique:true},
    phone       : { 
        type:String,
        validate :{
          validator: function(v:string){
            return /\d(3)-\d(3)-\d(4)/.test(v);
          },
          message : `the phone number isn't valid!`
        },
        required: [true, `User phone number required`]
    }
    },{timestamps:true});
  
export default model('user',UserSchema);